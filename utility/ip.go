package utility

import (
	"errors"
	"net"
	"runtime"
	"strings"
)

func GetIPV4Address() (string, error) {
	// If is windows...
	if runtime.GOOS == "windows" {
		addrs, err := net.InterfaceAddrs()
		if err != nil {
			return "", errors.New("Error getting IPV4 address. (net.InterfaceAddrs())")
		}
		for _, a := range addrs {
			if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
				if ipnet.IP.To4() != nil && !strings.HasPrefix(ipnet.IP.String(), "169.254.") {
					return ipnet.IP.String(), nil
				}
			}
		}
		return "", errors.New("Error getting IPV4 address.")
		// If is not windows...
	} else {
		ifaces, err := net.Interfaces()
		if err != nil {
			return "", err
		}
		for _, iface := range ifaces {
			if iface.Flags&net.FlagUp == 0 {
				continue // interface down
			}
			if iface.Flags&net.FlagLoopback != 0 {
				continue // loopback interface
			}
			addrs, err := iface.Addrs()
			if err != nil {
				return "", err
			}
			for _, addr := range addrs {
				var ip net.IP
				switch v := addr.(type) {
				case *net.IPNet:
					ip = v.IP
				case *net.IPAddr:
					ip = v.IP
				}
				if ip == nil || ip.IsLoopback() {
					continue
				}
				ip = ip.To4()
				if ip == nil {
					continue // not an ipv4 address
				}
				return ip.String(), nil
			}
		}
		return "", errors.New("are you connected to the network?")
	}
}
