package master

import (
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/siimu/vork/utility"
	"bitbucket.org/siimu/vork/workorder"

	"github.com/codegangsta/cli"
)

func CrackCommandHandler(c *cli.Context) {
	var (
		// nodes      []Node               - Global variable.
		replies      chan resourceReply      = make(chan resourceReply, 100)
		numOfReplies int                     = 0
		crackReplies chan workorder.Response = make(chan workorder.Response, 100)
		nodesFile    string                  = c.GlobalString("nodes")
		ip           string
		port         int           = c.GlobalInt("port")
		targetNodes  []Node        = make([]Node, 0)
		query        ResourceQuery // Query to be sent to workers for resource.
		me           Node
		id           string
		template     string
		hash         string
		wildcard     rune = rune(c.String("wildcard")[0])
		ttl          int  = c.Int("ttl")
	)

	//Arguments are in order: hash template
	args := c.Args()
	if len(args) < 2 {
		fmt.Println("[master] Too few arguments provided")
		return
	}
	hash = args.Get(0)
	template = args.Get(1)

	id, err := getRandomID()
	if err != nil {
		fmt.Println("[master] Failed to get ID for request")
		return
	}
	fmt.Printf("[master] Request assigned ID: %s\n", id)

	GetNodesFromFile(nodesFile)
	if len(nodes) == 0 {
		fmt.Println("[master] No known nodes. Bye!")
		return
	}

	ip, err = utility.GetIPV4Address()
	if err != nil {
		fmt.Printf("[master] Could not get machine's IP address: %s", err.Error())
		return
	}

	me = Node{IP: ip, Port: fmt.Sprintf("%d", port)}

	query = ResourceQuery{
		SendTo: me.IP,
		Port:   me.Port,
		TTL:    ttl,
		ID:     id,
		NoAsk:  getNoAsk(nodes),
	}

	setupListener(port, replies, crackReplies)

	err = query.SendToAll([]Node{})
	if err != nil {
		fmt.Printf("Error while trying to send: %s\n", err.Error())
		return
	}

	time.Sleep(time.Second * 3)

	targetNodes, numOfReplies = getTargetNodesFromReplies(replies)
	if numOfReplies > 0 {
		PutNodesToFile(nodesFile, targetNodes)
		err = sendWorkOrders(me, targetNodes, id, hash, template, wildcard)
		if err == nil {
			waitForAnswers(crackReplies, numOfReplies)
		}
	} else {
		fmt.Printf("Unfortunately we didn't get any replies.\n")
	}
	fmt.Printf("Program finished.\n")
}

func setupListener(port int, replies chan resourceReply, crackReplies chan workorder.Response) {
	http.HandleFunc("/resourcereply", func(w http.ResponseWriter, r *http.Request) {
		ServeResourceHTTP(w, r, replies)
	})
	http.HandleFunc("/answermd5", func(w http.ResponseWriter, r *http.Request) {
		ServeWorkOrderHTTP(w, r, crackReplies)
	})
	fmt.Printf("[master] Waiting for resource replies on port %d\n", port)
	go http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
}
