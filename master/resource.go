package master

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
)

//Represent a query for resources
type ResourceQuery struct {
	SendTo string
	Port   string
	TTL    int
	ID     string
	NoAsk  [][]string
}

//For recieving available nodes
type resourceReply struct {
	ID       string
	IP       string
	Port     int
	Resource int
}

func ServeResourceHTTP(responseWriter http.ResponseWriter, httpRequest *http.Request, replies chan resourceReply) {
	var reply resourceReply
	if httpRequest.Method != "POST" {
		http.Error(responseWriter, "Method not supported for resource query", http.StatusMethodNotAllowed)
		return
	}
	fmt.Printf("[master] Received resource reply from %s\n", httpRequest.RemoteAddr)

	decoder := json.NewDecoder(httpRequest.Body)
	err := decoder.Decode(&reply)
	if err != nil {
		fmt.Println("...invalid reply.")
		return
	}

	replies <- reply

	responseWriter.Write([]byte("0"))
}

//Send the resource query to all nodes known to this node, ignoring those in the blacklist
func (rq *ResourceQuery) SendToAll(blacklist []Node) error {
	whitelisted := []Node{}
	for _, node := range nodes {
		blacklisted := false
		for _, bnode := range blacklist {
			if node == bnode {
				blacklisted = true
			}
		}
		if !blacklisted {
			whitelisted = append(whitelisted, node)
		}
	}
	for _, node := range whitelisted {
		go rq.SendToSingle(node)
	}
	return nil
}

//Send the resource query to a single node
func (rq *ResourceQuery) SendToSingle(target Node) error {
	var (
		reqURL  string = fmt.Sprintf("http://%s/resource", target.URL())
		request *http.Request
		client  http.Client = http.Client{}
		values  url.Values  = make(url.Values)
	)
	request, err := http.NewRequest("GET", reqURL, nil)
	if err != nil {
		return err
	}
	fmt.Printf("[master] Sending resource query to %s\n", target.URL())
	values["sendip"] = []string{rq.SendTo}
	values["sendport"] = []string{rq.Port}
	values["ttl"] = []string{strconv.Itoa(rq.TTL)}
	if len(rq.ID) > 0 {
		values["id"] = []string{rq.ID}
	}
	if len(rq.NoAsk) > 0 {
		values["noask"] = []string{}
		for _, node := range rq.NoAsk {
			values["noask"] = append(values["noask"], fmt.Sprintf("%s_%s", node[0], node[1]))
		}
	}
	request.URL.RawQuery = values.Encode()
	_, err = client.Do(request)
	return err
}

func getNoAsk(nodes []Node) [][]string {
	var strings [][]string = make([][]string, 0)
	for _, node := range nodes {
		var nodeInfo []string = make([]string, 2)
		nodeInfo[0] = node.IP
		nodeInfo[1] = node.Port
		strings = append(strings, nodeInfo)
	}
	return strings
}

func getTargetNodesFromReplies(replies chan resourceReply) ([]Node, int) {
	var (
		numOfReplies int    = len(replies)
		newNodes     []Node = make([]Node, 0)
	)
	fmt.Printf("[master] Received %d replies.\n", numOfReplies)
	for i := 1; i <= numOfReplies; i++ {
		reply := <-replies
		//Nodes may answer several times for a single resource query
		newNode := Node{IP: reply.IP, Port: fmt.Sprintf("%d", reply.Port)}
		if !containsNode(newNodes, newNode) {
			newNodes = append(newNodes, newNode)
		}

	}
	fmt.Printf("[master] %d were unique nodes.\n", len(newNodes))
	return newNodes, numOfReplies
}
