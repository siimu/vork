package master

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
)

var nodes []Node = make([]Node, 0)

type Node struct {
	IP   string
	Port string
}

func (n Node) URL() string {
	return fmt.Sprintf("%s:%s", n.IP, n.Port)
}

func MakeNodes(nodes [][]string) []Node {
	var madeNodes []Node = make([]Node, 0)
	for _, node := range nodes {
		madeNodes = append(madeNodes, Node{IP: node[0], Port: node[1]})
	}
	return madeNodes
}

func GetNodesFromFile(filename string) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return
	}

	jsonString := string(data)
	dec := json.NewDecoder(strings.NewReader(jsonString))
	var strings [][]string
	for {
		if err := dec.Decode(&strings); err == io.EOF {
			break
		} else if err != nil {
			fmt.Println("Error parsing JSON.")
			return
		}
	}
	nodes = MakeNodes(strings)
}

func containsNode(nodes []Node, node Node) bool {
	for _, n := range nodes {
		if n == node {
			return true
		}
	}
	return false
}

func PutNodesToFile(filename string, nodesToAdd []Node) {
	var (
		// nodes  []Node  - These are global nodes.
		newNodes []Node = make([]Node, 0) // Nodes that will be written to a file.
		anyNew   bool   = false
	)

	for _, node := range nodes {
		newNodes = append(newNodes, node)
	}

	// Before appendig to slice, be sure that nothing is duplicated -
	// neither in global nodes nor new nodes.
	for _, node := range nodesToAdd {
		if !containsNode(nodes, node) && !containsNode(newNodes, node) {
			newNodes = append(newNodes, node)
			anyNew = true
		}
	}

	if len(newNodes) != 0 && anyNew {
		file, err := os.Create(filename)
		if err != nil {
			fmt.Printf("There was a problem writing nodes to file: %s", err.Error())
			return
		}
		defer file.Close()
		// Make nodes into two dimensional arrays.
		var stringsArray [][]string = make([][]string, 0)
		for _, node := range newNodes {
			var strings []string = []string{node.IP, node.Port}
			stringsArray = append(stringsArray, strings)
		}
		jsonString, _ := json.Marshal(stringsArray)
		file.Write(jsonString)
	}
}
