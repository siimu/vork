package master

import (
	"bytes"
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/siimu/vork/md5cracker"
	"bitbucket.org/siimu/vork/workorder"
)

func sendWorkOrders(me Node, targetNodes []Node, id string, hash string, template string, wildcard rune) error {
	var (
		divisions [][]string
	)

	divisions, err := md5cracker.Divide(template, wildcard, len(targetNodes))
	if err != nil {
		return errors.New(fmt.Sprintf("Could not get divisions: %s\n", err.Error()))
	}

	for i, target := range targetNodes {
		workOrder := workorder.Order{
			IP:       me.IP,
			Port:     me.Port,
			ID:       id,
			Hash:     hash,
			Ranges:   divisions[i],
			Wildcard: string(wildcard),
			Range:    [][]int{{0}}, // HARD-CODED
		}
		sendWorkOrderTo(workOrder, target)
	}
	return nil
}

func sendWorkOrderTo(order workorder.Order, target Node) {
	var (
		client  *http.Client = &http.Client{}
		reqBody *bytes.Reader
	)

	fmt.Printf("[master] Sending orders to %s:%s\n", target.IP, target.Port)

	if marshald, err := json.Marshal(order); err != nil {
		fmt.Printf("[master] Failed to send to %s. Encoding failure: %s\n", target.URL(), err.Error())
		return
	} else {
		reqBody = bytes.NewReader(marshald)
	}

	resp, err := client.Post(fmt.Sprintf("http://%s/checkmd5", target.URL()), "application/json", reqBody)
	if err != nil {
		fmt.Printf("[master] Failed to send to %s. Error on request: %s\n", target.URL(), err.Error())
		return
	}

	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		fmt.Printf("[master] Failed to send to %s (Returned code %d)\n", target.URL(), resp.StatusCode)
	}
}

func getRandomID() (string, error) {
	var (
		randomBytes []byte = make([]byte, 4)
		result      []byte = make([]byte, 8)
	)
	_, err := rand.Read(randomBytes)
	if err != nil {
		return "", err
	}
	hex.Encode(result, randomBytes)
	return string(result), nil
}

func waitForAnswers(crackReplies chan workorder.Response, numOfNodes int) {
	var (
		numOfAnswers int = 0
	)

	fmt.Printf("[master] Waiting for answers...\n")

	timeStarted := time.Now()
	for numOfAnswers < numOfNodes {
		if time.Now().Sub(timeStarted) < 5*time.Minute {
			if len(crackReplies) > 0 {
				numOfAnswers++
				reply := <-crackReplies
				fmt.Printf("%s: ", reply.URL())
				if reply.ResultCode == 1 {
					fmt.Printf("ANSWER FOUND - \"%s\"\n", reply.Result)
					return
				} else if reply.ResultCode == 2 {
					fmt.Printf("Timed out...\n")
				} else {
					fmt.Printf("Did not find...\n")
				}
			}
		} else {
			fmt.Printf("[master] Waiting timed out...\n")
			break
		}
	}
}

func ServeWorkOrderHTTP(responseWriter http.ResponseWriter, httpRequest *http.Request, replies chan workorder.Response) {
	var reply workorder.Response
	if httpRequest.Method != "POST" {
		http.Error(responseWriter, "Method not supported for resource query", http.StatusMethodNotAllowed)
		return
	}

	decoder := json.NewDecoder(httpRequest.Body)
	err := decoder.Decode(&reply)
	if err != nil {
		fmt.Println("...invalid reply.")
		return
	}

	replies <- reply

	responseWriter.Write([]byte("0"))
}
