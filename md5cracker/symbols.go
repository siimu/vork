package md5cracker

type symbolRange struct {
	Start, End rune
}

var symbolRanges []symbolRange = []symbolRange{
	symbolRange{rune(0), rune(0)},
	symbolRange{'0', '9'},
	symbolRange{'A', 'Z'},
	symbolRange{'a', 'z'},
}

func GetSymbolsSlice() []rune {

	var symbols []rune = make([]rune, 0)
	for _, r := range symbolRanges {
		for i := r.Start; i <= r.End; i++ {
			symbols = append(symbols, i)
		}
	}
	return symbols

}
