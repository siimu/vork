package md5cracker

import (
	"crypto/md5"
	"errors"
	"fmt"
	"io"
)

func replaceAtIndex(in string, r rune, i int) string {
	if r == 0 {
		return in[:i]
	}
	out := []rune(in)
	out[i] = r
	return string(out)
}

func tryHash(hash, word string) bool {
	newHash := Hash(word)
	if newHash == hash {
		return true
	} else {
		return false
	}
}

func containsChar(word string, letter rune) bool {
	for i := 0; i < len(word); i++ {
		if word[i] == byte(letter) {
			return true
		}
	}
	return false
}

func getFirstUnknownCharIndex(template string, unknownChar rune) (int, error) {
	for i := 0; i < len(template); i++ {
		if template[i] == byte(unknownChar) {
			return i, nil
		}
	}
	return -1, errors.New("Did not find any unknown chars.")
}

func Hash(word string) string {
	h := md5.New()
	io.WriteString(h, word)
	hash := fmt.Sprintf("%x", h.Sum(nil))
	return hash
}

func Crack(hash string, template string, unknownChar rune) (string, bool) {
	var (
		firstUnkownCharIndex int
		symbols              []rune = GetSymbolsSlice()
	)

	firstUnkownCharIndex, err := getFirstUnknownCharIndex(template, unknownChar)
	if err != nil {
		if tryHash(hash, template) {
			return template, true
		} else {
			return "", false
		}
	}

	for _, rune := range symbols {
		newString := replaceAtIndex(template, rune, firstUnkownCharIndex)
		if containsChar(newString, unknownChar) {
			result, found := Crack(hash, newString, unknownChar)
			if found {
				return result, true
			}
		} else if tryHash(hash, newString) {
			return newString, true
		}
	}

	return "", false
}

func Divide(template string, unknownChar rune, divisionCount int) ([][]string, error) {
	if divisionCount < 1 {
		return nil, errors.New("Division count cannot be 0 or less.")
	}
	var (
		firstUnkownCharIndex int
		symbols              []rune     = GetSymbolsSlice()
		symbolCount          int        = len(symbols)
		divisions            [][]string = make([][]string, 0)
		divisionSize         int        = (symbolCount + divisionCount - 1) / divisionCount
		divisionBegin        int        = 0
		divisionEnd          int        = divisionBegin + divisionSize
	)

	firstUnkownCharIndex, err := getFirstUnknownCharIndex(template, unknownChar)
	if err != nil {
		return nil, err
	}

	for i := 1; i <= divisionCount; i++ {
		var division []string = make([]string, 0)
		for j := divisionBegin; j < divisionEnd; j++ {
			newString := replaceAtIndex(template, symbols[j], firstUnkownCharIndex)
			division = append(division, newString)
		}
		divisions = append(divisions, division)
		divisionBegin = i * divisionSize
		divisionEnd = (i + 1) * divisionSize
		if divisionEnd > symbolCount {
			divisionEnd = symbolCount
		}
	}

	return divisions, nil
}
