package md5cracker

import (
	O "github.com/onsi/gomega"
	"testing"
)

func TestCracking(t *testing.T) {
	O.RegisterTestingT(t)

	hash := "68e1c85222192b83c04c0bae564b493d"
	found, result := Crack(hash, "ko???", '?')

	O.Ω(found).Should(O.BeTrue())
	O.Ω(result).Should(O.Equal("koer"))
}
