package workorder

import (
	"fmt"
)

//Notification to be sent on crack completion
type Response struct {
	IP         string `json:"ip"`
	Port       int    `json:"port"`
	ID         string `json:"id"`
	Hash       string `json:"md5"`
	ResultCode int    `json:"result"`
	Result     string `json:"resultstring,omitempty"`
}

func (r Response) URL() string {
	return fmt.Sprintf("%s:%d", r.IP, r.Port)
}
