package workorder

import (
	"fmt"
)

//Incoming workorder
type Order struct {
	IP       string   `json:"ip"`
	Port     string   `json:"port"`
	ID       string   `json:"ID"`
	Hash     string   `json:"md5"`
	Ranges   []string `json:"ranges"`
	Wildcard string   `json:"wildcard"`
	Range    [][]int  `json:"symbolrange"`
}

func (o Order) URL() string {
	return fmt.Sprintf("%s:%s", o.IP, o.Port)
}

//Check if workorder contains required fields
func (o Order) Valid() error {
	if len(o.IP) == 0 {
		return fmt.Errorf("Missing return IP!")
	}

	if len(o.Port) == 0 {
		return fmt.Errorf("Missing return port!")
	}

	if len(o.ID) == 0 {
		return fmt.Errorf("Missing work order ID")
	}

	if len(o.Hash) == 0 {
		return fmt.Errorf("Missing hash")
	}

	if len(o.Ranges) == 0 {
		return fmt.Errorf("Missing ranges")
	}
	return nil
}
