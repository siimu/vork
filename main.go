package main

import (
	"os"

	//Internal packages
	"bitbucket.org/siimu/vork/master"
	"bitbucket.org/siimu/vork/worker"

	//External dependencies
	"github.com/codegangsta/cli"
)

func main() {
	app := cli.NewApp()
	app.Name = "vork"
	app.Version = "0.1.0"
	app.Usage = `The P2Net protocol supporting MD5 "cracker"`

	app.Commands = []cli.Command{
		{
			Name:    "work",
			Aliases: []string{"w"},
			Usage:   "Listen for work and crack MD5 hashes when called upon",
			Action:  worker.WorkCommandHandler,
		},
		{
			Name:    "crack",
			Aliases: []string{"c"},
			Usage:   "Use the P2Net workers to crack your hash",
			Action:  master.CrackCommandHandler,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "wildcard,w",
					Value: "?",
					Usage: "The unknown character in the template",
				},
				cli.IntFlag{
					Name:  "ttl, t",
					Value: 10,
					Usage: "Time To Live for the request (in hops)",
				},
			},
		},
	}

	app.Flags = []cli.Flag{
		cli.IntFlag{
			Name:  "port,p",
			Value: 5089,
			Usage: "Port that should be used for HTTP",
		},
		cli.StringFlag{
			Name:  "nodes,n",
			Value: "machines.txt",
			Usage: "Nodes' IP and port are stored in that file",
		},
	}

	app.Authors = []cli.Author{
		{
			Name: "Priit Trink",
		},
		{
			Name:  "Siim Kaspar Uustalu",
			Email: "siim@pii.ee",
		},
	}

	app.Run(os.Args)
}
