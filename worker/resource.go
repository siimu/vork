package worker

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/siimu/vork/master"
	"bitbucket.org/siimu/vork/utility"

	"github.com/codegangsta/cli"
)

//Resource handler
type ResourceManager struct {
	Context   *cli.Context
	Available int
}

//Representation of incoming resource requests
type resourceRequest struct {
	Resource *ResourceManager
	SendTo   string
	Port     string
	TTL      int
	Id       string
	NoAsk    [][]string
}

//Representation of an outgoing resource availability notification
type resourceNotification struct {
	IP       string `json:"ip"`
	Port     int    `json:"port"`
	Id       string `json:"id,omitempty"`
	Resource int    `json:"resource"`
}

func (rr resourceRequest) Valid() error {
	if len(rr.SendTo) == 0 {
		return fmt.Errorf("Missing required field sendip")
	}
	if len(rr.Port) == 0 {
		return fmt.Errorf("Missing required field sendport")
	}
	if rr.TTL < 0 {
		return fmt.Errorf("Invalid TTL provided")
	}
	return nil
}

//Utility helper to format the URL better
func (rr *resourceRequest) URL() string {
	return fmt.Sprintf("%s:%s", rr.SendTo, rr.Port)
}

//Forward the resource request on to any non-blacklisted nodes we know
func (rr *resourceRequest) Forward() {
	noAsk := rr.NoAsk
	ip, err := utility.GetIPV4Address()
	if err != nil {
		fmt.Printf("[worker] Failed to forward resource request for %s (Failed to get own IPv4 addr: %s)", rr.URL(), err.Error())
	}
	port := strconv.Itoa(rr.Resource.Context.GlobalInt("port"))
	noAsk = append(noAsk, []string{ip, port})
	fwd := master.ResourceQuery{
		SendTo: rr.SendTo,
		Port:   rr.Port,
		TTL:    rr.TTL - 1,
		ID:     rr.Id,
		NoAsk:  noAsk,
	}
	blacklist := master.MakeNodes(noAsk)
	fwd.SendToAll(blacklist)
}

//Notify the requester of available resources
func (rr *resourceRequest) Notify() {
	var (
		client  *http.Client = &http.Client{}
		reqBody *bytes.Reader
	)

	if rr.TTL > 1 {
		defer rr.Forward()
	}

	fmt.Printf("[worker] Notifying %s of available resources\n", rr.URL())
	ip, err := utility.GetIPV4Address()
	if err != nil {
		fmt.Printf("[worker] Failed to notify %s of available resources (IP not found: %s)", err.Error())
		return
	}
	request := resourceNotification{
		Id:       rr.Id,
		Resource: rr.Resource.Available,
		IP:       ip,
		Port:     rr.Resource.Context.GlobalInt("port"),
	}
	if marshald, err := json.Marshal(request); err != nil {
		fmt.Printf("[worker] Failed to notify %s. Encoding failure: %s\n", rr.URL(), err.Error())
		return
	} else {
		reqBody = bytes.NewReader(marshald)
	}

	resp, err := client.Post(fmt.Sprintf("http://%s/resourcereply", rr.URL()), "application/json", reqBody)
	if err != nil {
		fmt.Printf("[worker] Failed to notify %s. Error on request: %s\n", rr.URL(), err.Error())
		return
	}

	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		fmt.Printf("[worker] Failed to notify %s (Returned code %d)\n", rr.URL(), resp.StatusCode)
	}
}

//Accept requests of resource availability notifications
func (r *ResourceManager) ServeHTTP(rw http.ResponseWriter, rq *http.Request) {
	if rq.Method != "GET" {
		http.Error(rw, "Method not supported for resource query", http.StatusMethodNotAllowed)
		return
	}
	fmt.Printf("[worker] Received resource query from %s\n", rq.RemoteAddr)

	// Update global nodes.
	master.GetNodesFromFile(r.Context.GlobalString("nodes"))

	rq.ParseForm()

	//Build a ResourceRequest from the received request
	rr := resourceRequest{Resource: r}
	if val, exists := rq.Form["sendip"]; exists && len(val) > 0 {
		rr.SendTo = val[0]
	}
	if val, exists := rq.Form["sendport"]; exists && len(val) > 0 {
		rr.Port = val[0]
	}
	if val, exists := rq.Form["id"]; exists && len(val) > 0 {
		rr.Id = val[0]
	}

	//Convert from string to int
	if val, exists := rq.Form["ttl"]; exists {
		if ttl, err := strconv.Atoi(val[0]); err == nil {
			rr.TTL = ttl
		}
	}

	if blacklist, exists := rq.Form["noask"]; exists {
		for _, pair := range blacklist {
			rr.NoAsk = append(rr.NoAsk, strings.Split(pair, "_"))
		}
		// New nodes found inside noAsk will be inserted to the file.
		master.PutNodesToFile(
			r.Context.GlobalString("nodes"),
			master.MakeNodes(rr.NoAsk),
		)
	}

	if err := rr.Valid(); err != nil {
		http.Error(rw, fmt.Sprintf("Bad request: %s", err.Error()), http.StatusBadRequest)
		return
	}

	rw.Write([]byte("0"))

	go rr.Notify()
}
