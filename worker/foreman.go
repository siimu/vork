package worker

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"bitbucket.org/siimu/vork/md5cracker"
	"bitbucket.org/siimu/vork/utility"
	"bitbucket.org/siimu/vork/workorder"
)

//Foreman is responsible for any cracking jobs running on the node
type Foreman struct {
	Manager *ResourceManager
}

func (f *Foreman) Launch(w workorder.Order) {
	var (
		wildcard rune = '?'
		found    bool = false
	)

	if len(w.Wildcard) > 0 {
		wildcard = rune(w.Wildcard[0])
	}

	timeStarted := time.Now()
	for _, template := range w.Ranges {
		if time.Now().Sub(timeStarted) < 4*time.Minute {
			if result, success := md5cracker.Crack(w.Hash, template, wildcard); success {
				f.Notify(w, 1, result)
				found = true
				break
			}
		} else {
			f.Notify(w, 2, "")
			f.Manager.Available = f.Manager.Available + (100 / MAXIMUM_TASKS)
			return
		}
	}

	if !found {
		f.Notify(w, 0, "")
	}

	f.Manager.Available = f.Manager.Available + (100 / MAXIMUM_TASKS)
}

func (f *Foreman) Notify(w workorder.Order, resultCode int, result string) {
	ip, err := utility.GetIPV4Address()
	if err != nil {
		log.Printf("[worker] Failed to notify %s of result. Couldn't get own IP: %s\n", w.URL(), err.Error())
		return
	}

	resp := workorder.Response{
		IP:         ip,
		Port:       f.Manager.Context.GlobalInt("port"),
		ID:         w.ID,
		Hash:       w.Hash,
		ResultCode: resultCode,
		Result:     result,
	}

	if resultCode == 1 {
		fmt.Printf("[worker] Notifying %s of successful result. (result string = %s)\n", w.URL(), resp.Result)
	} else if resultCode == 0 {
		fmt.Printf("[worker] Notifying %s of failure to find\n", w.URL())
	} else {
		fmt.Printf("[worker] Notifying %s of timeout\n", w.URL())
	}

	var (
		client  *http.Client = &http.Client{}
		reqBody *bytes.Reader
	)

	if marshald, err := json.Marshal(resp); err != nil {
		fmt.Printf("[worker] Failed to notify %s of result. Encoding failure: %s\n", w.URL(), err.Error())
		return
	} else {
		reqBody = bytes.NewReader(marshald)
	}

	req, err := client.Post(fmt.Sprintf("http://%s/answermd5", w.URL()), "application/json", reqBody)
	if err != nil {
		fmt.Printf("[worker] Failed to notify %s of result. Error on request: %s\n", w.URL(), err.Error())
		return
	}

	defer req.Body.Close()
	if req.StatusCode != http.StatusOK {
		fmt.Printf("[worker] Failed to notify %s of result (Returned code %d)\n", w.URL(), req.StatusCode)
	}
}

//Process incoming job requests
func (f Foreman) ServeHTTP(rw http.ResponseWriter, rq *http.Request) {
	var (
		order workorder.Order
	)
	if rq.Method != "POST" {
		http.Error(rw, "Method not supported for /checkmd5", http.StatusMethodNotAllowed)
		return
	}
	fmt.Printf("[worker] Received work order from %s\n", rq.RemoteAddr)

	dec := json.NewDecoder(rq.Body)

	defer rq.Body.Close()
	if err := dec.Decode(&order); err != nil {
		http.Error(rw, fmt.Sprintf("Failed to decode work order: %s", err.Error()), http.StatusBadRequest)
		return
	}

	if err := order.Valid(); err != nil {
		http.Error(rw, fmt.Sprintf("Invalid work order: %s", err.Error()), http.StatusBadRequest)
		return
	}
	fmt.Printf("[worker] Available resources %d\n", f.Manager.Available)
	if f.Manager.Available-(100/MAXIMUM_TASKS) > 0 {
		f.Manager.Available = f.Manager.Available - (100 / MAXIMUM_TASKS)
		go f.Launch(order)
		rw.Write([]byte("0"))
	} else {
		http.Error(rw, "No resource available", http.StatusInternalServerError)
		return
	}
}
