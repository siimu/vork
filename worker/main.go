package worker

import (
	"fmt"
	"net/http"

	"bitbucket.org/siimu/vork/master"

	"github.com/codegangsta/cli"
)

const (
	MAXIMUM_TASKS = 3 //Maximum simultaneous tasks
)

func WorkCommandHandler(c *cli.Context) {
	var (
		port      int             = c.GlobalInt("port")
		resources ResourceManager = ResourceManager{Available: 100, Context: c}
		nodesFile string          = c.GlobalString("nodes")
	)

	master.GetNodesFromFile(nodesFile)

	http.HandleFunc("/resource", func(rw http.ResponseWriter, rq *http.Request) {
		resources.ServeHTTP(rw, rq)
	})
	http.Handle("/checkmd5", Foreman{Manager: &resources})

	fmt.Printf("[worker] Waiting for work on port %d\n", port)
	http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
}
